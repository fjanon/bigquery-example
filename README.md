BigQuery Google App Engine (GAE) Java Example
=========================

Project goal
------------

This is an Eclipse GAE Java project for a servlet accessing BigQuery using a Service Account for authentication. It shows how to access Google's BigQuery from a GAE servlet (Java). Although not yet tested on another servlet container, the project should be pretty easy to port to a servlet container other than GAE, like Tomcat.

Overview
-----

The servlet doGet() sends simple queries to BigQuery to demonstrate how to use the BigQuery API. It displays some API call results in the logs and the query result on the web page as text. At the moment it doesn't stream data into BigQuery, the data file is first imported into the BigQuery dataset by uploading a csv file. It doesn't show the use of all BigQuery APIs, only a few.

The application accesses BigQuery using a Service Account. That means that the application doesn't require the user to authenticate to access the BigQuery dataset. It's done once when the servlet is loaded.

The project uses the Java App Engine SDK 1.9.3. It was tested with the versions of the SDK 1.8.9 through 1.9.3 without changes to the code. It uses only the BigQuery API, the client API (com.google.api.client.*) for authentication and the servlet API.

The project is an Eclipse Project. The important bits are the jars for Bigquery. If you create a new project, take the jars from this example. It took me a while to find the right jars and imports needed.

To access BigQuery our application requires:

- a Google account (for the developer to create a Google App Engine project and the BigQuery service)
- a GAE project
- enabling billing for the GAE project. Meaning a credit card although it will not be charged. There is a free quota on BigQuery that will allow you to run the project for free.
- enabling the BigQuery API for the GAE project
- credentials for the GAE project: the email address for the service account AND the credentials .p12 file
- creating a BigQuery dataset & table
- importing data into the dataset with a csv file

All the previous steps but the first are done in the GAE console at [https://console.developers.google.com](https://console.developers.google.com) . More details below.

The parameters used to send the BigQuery query are defined in web.xml as servlet init params: projectid, serviceaccountid and the credentials .p12 file path.

Steps
-----
1) GAE project: if you don't have an existing GAE project, create one and write down its name and projectId. It's done through the GAE console [https://console.developers.google.com/project](https://console.developers.google.com/project)

![GAE Project Created](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/01_Create_a_GAE_Project.png)

------------------------------------

2) Enabling billing: select your project in the dev console, then "Settings" and click on "Enable billing" if not enabled yet. Fill in the address and credit card info. You will get a runtime error if billing is not enabled for the project.


![Enable Billing](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/06_Enable_Billing.png)

------------------------------------

Billing limits:

![Billing Limits](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/08_Billing_Limits.png)

------------------------------------

3) Enabling the BigQuery API: in the GAE console select the project, click on "APIs & auth" and then "APIs". A page with the list of all the APIs appears. Find the "BigQuery API" row and click on the Status button (righmost column) to make it green ON. If it is already ON, just leave it.

![BigQuery_API_enabled](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/02_BigQuery_API_enabled.png)

------------------------------------

4) Creating a service account and getting its credentials: in your project page in the GAE console, select "APIs & Auth" then "Credentials". Under OAuth, click on "Create New Client ID". A window pops ups "Create Client ID", select "Service Account" and click on the "Create Client ID" button. It will ask you to download the credentials file. Store it in your local Eclipse project /WEB-INF/ folder with the file name "privatekey.p12". Write down the private key password as indicated in the console dialog (you actually don't need it for this project) and make a note of the service account email, something like "xxxxx@developer.gserviceaccount.com"

 NOTE: If you get an error "Server Error: An internal error occured: ... Internal robots may not specify public certs", follow the same procedure again and select a "Web Application" first, create it and try again for a "Service Account".

![BigQuery Create Credential](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/10_BigQuery_Create_Credential.png)

------------------------------------

Service Account:

![BigQuery Create Credential](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/11_BigQuery_Create_Credential.png)

------------------------------------

5) In the GAE console, check that the service account email has been added to the project "Permissions", if not add it, paste it in the "add member" dialog.

![BigQuery Permissions](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/12_BigQuery_Permissions.png)

------------------------------------

Service Account Credential Error:

![Credential Error](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/13_Credential_Error.png)

------------------------------------

Key Pair Generated:

![Key Pair Generated](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/14_Public_Key_generated.png)

------------------------------------

6) open the "web.xml" file in the project and copy your own project ID, service account email in the corresponding servlet init params. In the "appengine-web.xml" set your project name to be able to deploy it to the GAE.

7) Create the BigQuery dataset & table: in the GAE console for your project, click on the "BigQuery" with the looking glass under "Quotas", that leads you to [https://bigquery.cloud.google.com/project/yourprojectname](https://bigquery.cloud.google.com/project/yourprojectname) . Create a dataset named "company_employees" and a table "employees" by clicking on the down arrow menu, right side of the project name.

![Create Dataset](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/21a_Create_Dataset.png)

------------------------------------

![Dataset Created](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/21b_Database_Created.png)

------------------------------------

![Create Table](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/22_Create_Table.png)

------------------------------------

![Create Table](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/23_Create_Table.png)

------------------------------------

8) import the employees.csv file in the employees table of the company dataset.

For our example, we'll upload a short .csv file already in the project, but you can use a JSON file or an AppEngine Datastore Backup as a data source. It is a convenient way for projects with the data already stored in the GAE datastore to copy data directly into BigQuery tables.

To upload the project .csv file, in the BigQuery console, move the mouse cursor onto the dataset title "company_employees", a "+" icon shows up on the right side of the dataset title, click on it and the dialog for data upload is displayed. If you have uploaded some data before, BigQuery remembers it and will save you some time, there will be a "Job Template" tab you can use. You can skip that step and get to the "Choose destination" tab, where you enter the dataset & table IDs that we need to upload data into. In this case: "Dataset ID: company_employees" and "Table ID: employees".

![Import Table Data](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/24_Import_Table_Data.png)

------------------------------------

Click on "Next" to get to the "Select data" tab then select "Source Format: CSV", "File upload: choose file" and browse to the project "employees.csv" file. Click on "Next" to get to the "Specify Schema" tab and enter or copy in the "Schema" input field: "name:string,number:integer,department:string,city:string" without the "" quotes.

![Specify Schema](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/25_Specify_Schema.png)

------------------------------------

![Table Fields Schema](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/27_Table_Fields_Schema.png)

------------------------------------

Click on "Next" to get to the "Advanced Options" tab, leave the options as is, click on "Submit".

![Import Options](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/28_Import_Options.png)

------------------------------------

The import job will start and can last up to a minute, even with a very small file. The BigQuery console will show the "employees (loading)" status and "Job History (1 running)".

![CSV Loading](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/30_CSV_Loading.png)

------------------------------------

![CSV Loading](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/32_Load_Pending.png)

------------------------------------

![Table Loading](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/33_Table_Loading.png)

------------------------------------

If the csv file is not properly formatted, which should not be the case in our project file, you can check the cause of the error by looking at the "Job History" in the BigQuery console: click on "Job History" and then on the job in error with a red icon.

![CSV Load Error](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/36_CSV_Load_Error.png)

------------------------------------

After the blue "Pending" job status, when the import succeeds, you get the green checkmark on the job.

![CSV Loaded OK](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/35_CSV_Loaded_OK.png)

------------------------------------

You can check the data by clicking on the "employees" table title (left side) and then on the "details" button (right side) once the table info shows up. The preview is displayed with the data from the csv file.

![Employees Table Details](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/40_Employees_Table_Details.png)

------------------------------------

9) compose a query in the BigQuery console:

Note: this step is optional since the query is already defined in the project code, but recommended.

Click on "Compose Query" (top left side). In the "New Query" filed, input: "SELECT number FROM [company_employees.employees] LIMIT 1000" without the "" quotes. After a few seconds (could be 5 to 10 the first time), the results are displayed.

![Employees Query](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/42_Employees_Query.png)

------------------------------------

![Console Query Results](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/44_Console_Query_Results.png)

------------------------------------

That shows how to compose a new query and debug it with the console. In our project, the query is already set in the project, as a servlet init param in web.xml so it can be changed easily.

You can check the BigQuery resource usage:

![BigQuery Resource Usage](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/48_BigQuery_Resource_Usage.png)

------------------------------------

10) The Bigquery dataset and table are all set, the BigQuery parameters defined in web.xml, we can now run the project locally to debug it and have a look at the console logs. It should show that the servlet init has run without errors. If there is one, use the log to fix it. Then go to [http://localhost:8888/bigquery](http://localhost:8888/bigquery) . Wait for a few seconds and the JSON response from BigQuery should show, pretty similar to that, the first time cacheHit will be false:

~~~~
{
  "cacheHit" : true,
  "jobComplete" : true,
  "jobReference" : {
    "jobId" : "job_Q-TtbC1CmLTCAzKR6ZCobE-M9j8",
    "projectId" : "red-octane"
  },
  "kind" : "bigquery#queryResponse",
  "rows" : [ {
    "f" : [ {
      "v" : "Schupp"
    }, {
      "v" : "11"
    } ]
  }, {
    "f" : [ {
      "v" : "Whisnant"
    }, {
      "v" : "22"
    } ]
  }, {
    "f" : [ {
      "v" : "Ehrmann"
    }, {
      "v" : "33"
    } ]
  }, {
    "f" : [ {
      "v" : "Garneau"
    }, {
      "v" : "44"
    } ]
  } ],
  "schema" : {
    "fields" : [ {
      "mode" : "NULLABLE",
      "name" : "name",
      "type" : "STRING"
    }, {
      "mode" : "NULLABLE",
      "name" : "number",
      "type" : "INTEGER"
    } ]
  },
  "totalBytesProcessed" : "0",
  "totalRows" : "4"
}
~~~~

11) Make sure your application ID (it's the project id) is set in the project deployment options. It gets written in appengine-web.xml.

![Deploy Options](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/50_Deploy_Options.png)

------------------------------------

12) deploy the project on the GAE, using the Google Menu (blue 'G' icon).

![AppEngine Deployment](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/51_AppEngine_Deployment.png)

------------------------------------

Eclipse will open a browser tab for you, click on BigQuery and you should see the same ouput as before, only it's now running on the Google App Engine.

------------------------------------

![GAE Log](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/60_GAE_Log.png)

------------------------------------

![GAE Log](http://bytebucket.org/fjanon/bigquery-example/raw/master/screenshots/61_GAE_Log.png)

------------------------------------

Notes:
-----
- Even deployed on the GAE, the query response time I see is at least 1.1 to 1.5 seconds with the very small dataset we have here. Don't expect to build an app with a sub-second query response time. BigQuery will execute a query over a *huge* dataset within several seconds, that's what it's built for.

- If you want to see the exchanges between the app and BigQuery, use ".level = CONFIG" in logging.properties

If something goes wrong:
------------------------

Check:

- your projectid in app-engine.xml for the deployment on GAE
- your projectid in web.xml for the servlet to access BigQuery
- your service account email in web.xml
- run the project locally and check the console log. The clues are there.

References:
-----------
- [What is BigQuery](https://developers.google.com/bigquery/what-is-bigquery)
- [BigQuery: Getting started with Java](https://developers.google.com/bigquery/articles/gettingstartedwithjava)
- [Codelab Getting Started with BigQuery and the Google Java API Client library](https://developers.google.com/bigquery/articles/gettingstartedwithjava) This example uses a Client ID for authentication not a Service Account.
- [OAuth2 Developer's Guide](https://code.google.com/p/google-api-java-client/wiki/OAuth2)
- [OAuth authorization](https://developers.google.com/bigquery/authorization)
- [OAuth2](https://developers.google.com/console/help/new/#generatingoauth2)
- [Querying data](https://developers.google.com/bigquery/querying-data)
- [BigQuery API Quickstart](https://google-developers.appspot.com/bigquery/bigquery-api-quickstart)
- [BigQuery API Client Library for Java Quickstart](https://developers.google.com/api-client-library/java/apis/bigquery/v2)
- [BigQuery Java API](https://developers.google.com/resources/api-libraries/documentation/bigquery/v2/java/latest/)
- [Bigquery Datasets API](https://developers.google.com/resources/api-libraries/documentation/bigquery/v2/java/latest/com/google/api/services/bigquery/Bigquery.Datasets.html)


Thanks to:
----------
- Martin Görner, Google Dev Advocate
- the Google Cloud Platform team
- Dillinger, the cloud-enabled HTML5 Markdown editor http://dillinger.io/
